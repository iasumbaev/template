$(document).ready(function () {
    $('.custom-file-input').on('change', function (event) {
        var inputFile = event.currentTarget;
        $(inputFile).parent()
            .find('.custom-file-label')
            .html(inputFile.files[0].name);
    });
    $('.delete-btn').click(function () {
        $('.modal-delete-btn').attr('href', $(this).data('href'));
    });
});