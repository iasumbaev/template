<?php


namespace App\Service;


use App\Entity\Image;
use Gedmo\Sluggable\Util\Urlizer;
use League\Flysystem\AdapterInterface;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploaderHelper
{
    private $filesystem;


    private $requestStackContext;

    private $logger;

    private $publicAssetBaseUrl;

    public function __construct(FilesystemInterface $uploadsFilesystem, RequestStackContext $requestStackContext, LoggerInterface $logger, string $uploadedAssetsBaseUrl)
    {
        $this->filesystem = $uploadsFilesystem;
        $this->requestStackContext = $requestStackContext;
        $this->logger = $logger;
        $this->publicAssetBaseUrl = $uploadedAssetsBaseUrl;
    }

    /**
     * @param File $file
     * @param string|null $existingFilename
     * @param $directory
     * @return string
     */
    public function uploadFile(File $file, ?string $existingFilename, $directory): string
    {
        $newFilename = $this->upload($file, $directory, true);

        if ($existingFilename) {
            try {
                $result = $this->filesystem->delete($directory . '/' . $existingFilename);

                if ($result === false) {
                    throw new RuntimeException(sprintf('Could not delete old uploaded file "%s"', $existingFilename));
                }
            } catch (FileNotFoundException $e) {
                $this->logger->alert(sprintf('Old uploaded file "%s" was missing when trying to delete', $existingFilename));
            }
        }

        return $newFilename;
    }

    /**
     * @param File $file
     * @param string|null $existingFilename
     * @param $directory
     * @return Image
     */
    public function uploadImage(File $file, ?string $existingFilename, $directory): Image
    {
        $newFilename = $this->uploadFile($file, $existingFilename, $directory);
        $image = new Image();
        if ($file instanceof UploadedFile) {
            $originalFilename = $file->getClientOriginalName();
        } else {
            $originalFilename = $file->getFilename();
        }
        $image
            ->setFilename($newFilename)
            ->setOriginalFilename($originalFilename)
            ->setViewFilename($originalFilename)
            ->setMimeType($file->getMimeType() ?? 'application/octet-stream')
            ->setExtension($file->getClientOriginalExtension())
            ->setSize($file->getSize());

        return $image;
    }

    public function getPublicPath(string $path): string
    {
        $fullPath = $this->publicAssetBaseUrl . '/' . $path;
        // if it's already absolute, just return
        if (strpos($fullPath, '://') !== false) {
            return $fullPath;
        }
        // needed if you deploy under a subdirectory
        return $this->requestStackContext
                ->getBasePath() . $fullPath;
    }

    public function deleteFile(string $path)
    {
        try {
            $result = $this->filesystem->delete($path);
            if ($result === false) {
                throw new RuntimeException(sprintf('Error deleting "%s"', $path));
            }
        } catch
        (FileNotFoundException $e) {
            $this->logger->alert(sprintf('File "%s" was missing when trying to delete', $path));
        }
    }

    private function upload(File $file, string $directory, bool $isPublic): string
    {
        if ($file instanceof UploadedFile) {
            $originalFilename = $file->getClientOriginalName();
        } else {
            $originalFilename = $file->getFilename();
        }

        $newFilename = Urlizer::urlize(pathinfo($originalFilename, PATHINFO_FILENAME)) . '-' . uniqid('', true) . '.' . $file->guessExtension();

        $stream = fopen($file->getPathname(), 'r');
        $result = $this->filesystem->writeStream(
            $directory . '/' . $newFilename,
            $stream,
            [
                'visibility' => $isPublic ? AdapterInterface::VISIBILITY_PUBLIC : AdapterInterface::VISIBILITY_PRIVATE
            ]
        );

        if ($result === false) {
            throw new RuntimeException(sprintf('Could not write uploaded file "%s"', $newFilename));
        }

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $newFilename;
    }


}