/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection'] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'HorizontalRule,SpecialChar,RemoveFormat,Strike,Maximize,Styles,Format,Underline,Subscript,Superscript,Cut,Paste,Source,Copy,PasteFromWord,PasteText,SpellChecker,Anchor,Table';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';
	config.removePlugins='image';
	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:Link;image:advanced;link:advanced;link:upload;';
	config.filebrowserUploadMethod  = "form";
	config.autoParagraph = false;
	config.enterMode = CKEDITOR.ENTER_BR; // pressing the ENTER KEY input <br/>
	config.shiftEnterMode = CKEDITOR.ENTER_P; //pressing the SHIFT + ENTER KEYS input <p>
};
