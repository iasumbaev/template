<?php


namespace App\Controller;


use App\Service\FormSubmitHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

trait AdminControllerTrait
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var FormSubmitHelper
     */
    private $formSubmitHelper;

    public function __construct(EntityManagerInterface $entityManager, FormSubmitHelper $formSubmitHelper)
    {
        $this->entityManager = $entityManager;
        $this->formSubmitHelper = $formSubmitHelper;
    }

    private function initForm($type, $data, Request $request): FormInterface
    {
        /**
         * @var FormInterface $form
         */
        $form = $this->createForm($type, $data);
        $form->handleRequest($request);
        return $form;
    }

    private function renderOrRedirect($type, $data, Request $request, string $redirectRoute, string $renderView, array $redirectParameters = [], array $renderParameters = [], bool $hasImage = false, ?string $classType = null)
    {
        $form = $this->initForm($type, $data, $request);
        if ($this->formSubmitHelper->validateAndSubmitForm($form, $data, $hasImage, $classType)) {
            $this->addFlash('success', 'Изменения успешно сохранены');
            return $this->redirectToRoute($redirectRoute,
                $redirectParameters);
        }
        $renderParameters['form'] = $form->createView();
        return $this->render($renderView, $renderParameters);
    }
}