$(document).ready(function () {
    function onDrop($item, container) {
        $item.removeClass(container.group.options.draggedClass).removeAttr("style");
        $("body").removeClass(container.group.options.bodyClass);
        var ids = {};
        var $container = $item.parent();
        $container.find('tr').each(function (index) {
            ids[index] = ($(this).data('id'));
        });
        console.log(ids);
        $.ajax(
            {
                type: "POST",
                url: $container.data('url'),
                data: ids,
                success: function (data) {
                    console.log(data);
                }
            }
        );
    }

    $(".sortable").sortable({
        onDrop: onDrop,
        group: 'no-drop',
        handle: 'i.fa-arrows-alt-v',
        containerSelector: 'table',
        itemPath: '> tbody',
        itemSelector: 'tr',
        placeholder: '<tr class="placeholder"/>'
    });
});