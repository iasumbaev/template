<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FormSubmitHelper
{
    /**
     * @var UploaderHelper
     */
    private $uploaderHelper;
    /**
     * @var HelpService
     */
    private $helpService;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AdminSliderController constructor.
     * @param UploaderHelper $uploaderHelper
     * @param HelpService $helpService
     */
    public function __construct(UploaderHelper $uploaderHelper, HelpService $helpService, EntityManagerInterface $entityManager)
    {
        $this->uploaderHelper = $uploaderHelper;
        $this->helpService = $helpService;
        $this->entityManager = $entityManager;
    }

    private function submitImage(FormInterface $form, $data, $class): void
    {
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $form['imageFile']->getData();
        if ($uploadedFile) {
            if (method_exists($data, 'getImageFilename')) {
                $newFilename = $this->uploaderHelper->uploadFile($uploadedFile, $data->getImageFilename(), $this->helpService->getSnakeClassName($class));
            } else {
                throw new LogicException('$data has not method getImageFilename');
            }

            if (method_exists($data, 'setImageFilename')) {
                $data->setImageFilename($newFilename);
            } else {
                throw new LogicException('$data has not method setImageFilename');
            }
        }
    }

    public function validateAndSubmitForm(FormInterface $form, $data, bool $hasImage = false, ?string $classType = null)
    {
        if ($form->isSubmitted() && $form->isValid()) {

            if ($hasImage) {
                $this->submitImage($form, $data, $classType);
            }

            $this->entityManager->persist($data);
            $this->entityManager->flush();
            return true;
        }

        return false;
    }
}